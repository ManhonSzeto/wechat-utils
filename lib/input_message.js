'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Link = exports.Location = exports.ShortVideo = exports.Video = exports.Voice = exports.Image = exports.Text = undefined;

var _message_schema = require('./message_schema');

var Schema = _interopRequireWildcard(_message_schema);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

class Text extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Content', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.Text = Text;
class Image extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'PicUrl', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.Image = Image;
class Voice extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'Format', true);
        Schema.Schema.defineElement(this, 'Recognition', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.Voice = Voice;
class Video extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'ThumbMediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.Video = Video;
class ShortVideo extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'ThumbMediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.ShortVideo = ShortVideo;
class Location extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Location_X');
        Schema.Schema.defineElement(this, 'Location_Y');
        Schema.Schema.defineElement(this, 'Scale');
        Schema.Schema.defineElement(this, 'Label', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

exports.Location = Location;
class Link extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Title', true);
        Schema.Schema.defineElement(this, 'Description', true);
        Schema.Schema.defineElement(this, 'Url', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}
exports.Link = Link;