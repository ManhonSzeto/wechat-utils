import * as HttpUtils from 'http-client-utils';

export async function getAccessToken(appID, appSecret) {
    const url = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appID}&secret=${appSecret}`;
    return await HttpUtils.getJSON(url);
}

export async function getJSAPITicket(accessToken) {
    const url = `https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=${accessToken}&type=jsapi`;
    return await HttpUtils.getJSON(url);
}

export async function getCallbackIP(accessToken) {
    const url = `https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=${accessToken}`;
    return await HttpUtils.getJSON(url);
}

export async function menuCreate(accessToken, menuData) {
    const url = ` https://api.weixin.qq.com/cgi-bin/menu/create?access_token=${accessToken}`;
    return await HttpUtils.postJSON(url, menuData);
}

export async function menuGet(accessToken) {
    const url = `https://api.weixin.qq.com/cgi-bin/menu/get?access_token=${accessToken}`;
    return await HttpUtils.getJSON(url);
}

export async function menuDelete(accessToken) {
    const url = `https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=${accessToken}`;
    return await HttpUtils.getJSON(url);
}

export async function customSend(accessToken, customMessage) {
    const url = `https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=${accessToken}`;
    return await HttpUtils.postJSON(url, customMessage);
}

export async function getUserAccessToken(appID, appSecret, code) {
    const url = `https://api.weixin.qq.com/sns/oauth2/access_token?appid=${appID}&secret=${appSecret}&code=${code}&grant_type=authorization_code`;
    return await HttpUtils.getJSON(url);
}

export async function refreshUserAccessToken(appID, userRefreshToken) {
    const url = `https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=${appID}&grant_type=refresh_token&refresh_token=${userRefreshToken}`;
    return await HttpUtils.getJSON(url);
}

// lang : zh_CN, zh_TW, en
export async function getUserInfo(userAccessToken, openID, lang = 'zh_CN') {
    const url = `https://api.weixin.qq.com/sns/userinfo?access_token=${userAccessToken}&openid=${openID}&lang=${lang}`;
    return await HttpUtils.getJSON(url);
}

export async function checkUserAccessToken(userAccessToken, openID) {
    const url = `https://api.weixin.qq.com/sns/auth?access_token=${userAccessToken}&openid=${openID}`;
    return await HttpUtils.getJSON(url);
}

export async function downloadTemporaryMediaResource(mediaID, accessToken, filePath) {
    const url = `https://api.weixin.qq.com/cgi-bin/media/get?access_token=${accessToken}&media_id=${mediaID}`;
    await HttpUtils.download(url, filePath);
}

export async function payUnifiedOrder(xml) {
    const url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
    return await HttpUtils.postXML(url, xml);
}

export async function sendTemplateMessage(accessToken, templateMessage) {
    const url = `https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=${accessToken}`
    return await HttpUtils.postJSON(url, templateMessage);
}
