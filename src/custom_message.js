class CustomMessageSchema {
    constructor(openID, customServiceAccount = null) {
        this._holder = {
            touser: openID
        };
        if (customServiceAccount !== undefined && customServiceAccount !==
            null) {
            this._holder.customservice = {
                kf_account: customServiceAccount
            }
        }
    }

    JSON() {
        return this._holder;
    }
}

export class Text extends CustomMessageSchema {
    constructor(openID, text, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'text';
        this._holder.text = {
            content: text
        };
    }
}

export class Image extends CustomMessageSchema {
    constructor(openID, mediaID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'image';
        this._holder.image = {
            media_id: mediaID
        }
    }
}

export class Voice extends CustomMessageSchema {
    constructor(openID, mediaID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'voice';
        this._holder.voice = {
            media_id: mediaID
        }
    }
}

export class Video extends CustomMessageSchema {
    constructor(openID, mediaID, thumbMediaID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'video';
        this._holder.video = {
            media_id: mediaID,
            thumb_media_id: thumbMediaID
        }
    }

    set title(value) {
        this._holder.video.title = value;
    }

    set description(value) {
        this._holder.video.description = value;
    }
}

export class Music extends CustomMessageSchema {
    constructor(openID, musicURL, HQMusicURL, thumbMediaID,
        customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'music';
        this._holder.music = {
            musicurl: musicURL,
            hqmusicurl: HQMusicURL,
            thumb_media_id
        }
    }

    set title(value) {
        this._holder.music.title = value;
    }

    set description(value) {
        this._holder.music.description = value;
    }
}

export class News extends CustomMessageSchema {
    constructor(openID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'news';
        this._holder.news = {
            articles: []
        }
    }

    addNews(title, description, url, picURL) {
        let testTarget = false;

        const newsObj = {};

        if (title !== undefined && title !== null) {
            newsObj.title = title;
            testTarget = true;
        }

        if (description !== undefined && description !== null) {
            newsObj.description = description;
            testTarget = true;
        }

        if (url !== undefined && url !== null) {
            newsObj.url = url;
            testTarget = true;
        }

        if (picURL !== undefined && picURL !== null) {
            newsObj.picurl = picURL;
            testTarget = true;
        }

        if (testTarget) {
            this._holder.news.articles.push(newsObj);
        }
    }
}

export class MPNews extends CustomMessageSchema {
    constructor(openID, mediaID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'mpnews';
        this._holder.mpnews = {
            media_id: mediaID
        }
    }
}

export class WechatCard extends CustomMessageSchema {
    constructor(openID, cardID, customServiceAccount = null) {
        super(openID, customServiceAccount);
        this._holder.msgtype = 'wxcard';
        this._holder.wxcard = {
            card_id: cardID
        }
    }
}
