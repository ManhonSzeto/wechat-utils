import * as Schema from './message_schema';

export class Subscribe extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
        Schema.Schema.defineElement(this, 'Ticket', true);
    }
}

export class Unsubscribe extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
    }
}

export class Scan extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
        Schema.Schema.defineElement(this, 'Ticket', true);
    }
}

export class Location extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'Latitude');
        Schema.Schema.defineElement(this, 'Longitude');
        Schema.Schema.defineElement(this, 'Precision');
    }
}

export class Click extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
    }
}

export class View extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
    }
}

export class ScancodePush extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
        Schema.Schema.defineChildElement(this, 'ScanCodeInfo',
            'ScanType');
        Schema.Schema.defineChildElement(this, 'ScanCodeInfo',
            'ScanResult');
    }
}

export class ScancodeWaitmsg extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
        Schema.Schema.defineChildElement(this, 'ScanCodeInfo',
            'ScanType');
        Schema.Schema.defineChildElement(this, 'ScanCodeInfo',
            'ScanResult');
    }
}

export class PicSysphoto extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
    }
}

export class PicPhotoOrAlbum extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
    }
}

export class PicWeixin extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
    }
}


export class LocationSelect extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'EventKey', true);
        Schema.Schema.defineChildElement(this, 'SendLocationInfo',
            'Location_X');
        Schema.Schema.defineChildElement(this, 'SendLocationInfo',
            'Location_Y');
        Schema.Schema.defineChildElement(this, 'SendLocationInfo',
            'Scale');
        Schema.Schema.defineChildElement(this, 'SendLocationInfo',
            'Label');
        Schema.Schema.defineChildElement(this, 'SendLocationInfo',
            'Poiname');
    }
}

export class CardPassCheck extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
    }
}

export class UserGetCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'IsGiveByFriend');
        Schema.Schema.defineElement(this, 'UserCardCode', true);
        Schema.Schema.defineElement(this, 'OuterId');
        Schema.Schema.defineElement(this, 'OuterStr', true);
    }
}

export class UserDelCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
    }
}

export class UserConsumeCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
        Schema.Schema.defineElement(this, 'ConsumeSource', true);
        Schema.Schema.defineElement(this, 'OutTradeNo', true);
        Schema.Schema.defineElement(this, 'TransId', true);
        Schema.Schema.defineElement(this, 'LocationId', true);
        Schema.Schema.defineElement(this, 'StaffOpenId', true);
    }
}

export class UserPayFromPayCell extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
        Schema.Schema.defineElement(this, 'TransId', true);
        Schema.Schema.defineElement(this, 'LocationId', true);
        Schema.Schema.defineElement(this, 'Fee', true);
        Schema.Schema.defineElement(this, 'OriginalFee', true);
    }
}

export class UserViewCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
        Schema.Schema.defineElement(this, 'OuterStr', true);
    }
}

export class UserEnterSessionFromCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
    }
}

export class UpdateMemberCard extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
        Schema.Schema.defineElement(this, 'ModifyBonus');
        Schema.Schema.defineElement(this, 'ModifyBalance');
    }
}

export class CardSkuRemind extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'Detail', true);
    }
}

export class CardPayOrder extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'OrderId', true);
        Schema.Schema.defineElement(this, 'Status', true);
        Schema.Schema.defineElement(this, 'CreateOrderTime');
        Schema.Schema.defineElement(this, 'PayFinishTime');
        Schema.Schema.defineElement(this, 'Desc', true);
        Schema.Schema.defineElement(this, 'FreeCoinCount', true);
        Schema.Schema.defineElement(this, 'PayCoinCount', true);
        Schema.Schema.defineElement(this, 'RefundFreeCoinCount', true);
        Schema.Schema.defineElement(this, 'RefundPayCoinCount', true);
        Schema.Schema.defineElement(this, 'OrderType', true);
        Schema.Schema.defineElement(this, 'Memo', true);
        Schema.Schema.defineElement(this, 'ReceiptInfo', true);
    }
}

export class SubmitMembercardUserInfo extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Event', true);
        Schema.Schema.defineElement(this, 'CardId', true);
        Schema.Schema.defineElement(this, 'UserCardCode', true);
    }
}
