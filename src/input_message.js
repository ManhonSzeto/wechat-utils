import * as Schema from './message_schema';

export class Text extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Content', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class Image extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'PicUrl', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class Voice extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'Format', true);
        Schema.Schema.defineElement(this, 'Recognition', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class Video extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'ThumbMediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class ShortVideo extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'MediaId', true);
        Schema.Schema.defineElement(this, 'ThumbMediaId', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class Location extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Location_X');
        Schema.Schema.defineElement(this, 'Location_Y');
        Schema.Schema.defineElement(this, 'Scale');
        Schema.Schema.defineElement(this, 'Label', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}

export class Link extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime');
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Title', true);
        Schema.Schema.defineElement(this, 'Description', true);
        Schema.Schema.defineElement(this, 'Url', true);
        Schema.Schema.defineElement(this, 'MsgId');
    }
}
