import * as Schema from './message_schema';

export class TransfersRequest extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'mch_appid');
        Schema.Schema.defineElement(this, 'mchid');
        Schema.Schema.defineElement(this, 'device_info');
        Schema.Schema.defineElement(this, 'nonce_str');
        Schema.Schema.defineElement(this, 'sign');
        Schema.Schema.defineElement(this, 'partner_trade_no');
        Schema.Schema.defineElement(this, 'openid');
        Schema.Schema.defineElement(this, 'check_name');
        Schema.Schema.defineElement(this, 're_user_name');
        Schema.Schema.defineElement(this, 'amount');
        Schema.Schema.defineElement(this, 'desc');
        Schema.Schema.defineElement(this, 'spbill_create_ip');
    }
}

export class TransfersResponse extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'return_code', true);
        Schema.Schema.defineElement(this, 'return_msg', true);
        Schema.Schema.defineElement(this, 'mch_appid', true);
        Schema.Schema.defineElement(this, 'mch_id', true);
        Schema.Schema.defineElement(this, 'device_info', true);
        Schema.Schema.defineElement(this, 'nonce_str', true);
        Schema.Schema.defineElement(this, 'result_code', true);
        Schema.Schema.defineElement(this, 'err_code', true);
        Schema.Schema.defineElement(this, 'err_code_des', true);
        Schema.Schema.defineElement(this, 'partner_trade_no', true);
        Schema.Schema.defineElement(this, 'payment_no', true);
        Schema.Schema.defineElement(this, 'prepay_id', true);
        Schema.Schema.defineElement(this, 'payment_time', true);
    }
}
