export default class {
    constructor() {
        this._messageHandlers = new Map();
        this._eventHandlers = new Map();
        this._registerID = 0;
    }

    registerMessageHandler(type, handler) {
        const registerID = ++this._registerID;


        return registerID;
    }

    registerEventHandler(type, handler) {
        const registerID = ++this._registerID;


        return registerID;
    }

    unregisterMessageHandler(registerID) {

    }

    unregisterEventHandler(registerID) {

    }

    dispatch(xmlObject) {

    }
}
