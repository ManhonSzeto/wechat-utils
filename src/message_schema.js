import Convertor from 'x2js';

export class Schema {
    constructor() {
        this._convertor = new Convertor();
        this._holder = {
            xml: {}
        };
    }

    fromString(xmlString) {
        this._holder = this._convertor.xml2js(xmlString);
    }

    fromObject(xmlObject) {
        this._holder = xmlObject;
    }

    toObject() {
        if (this._holder === null || this._holder === undefined) {
            throw new Error(`Text holder is not inited`);
            return null;
        }
        return this._holder;
    }

    toString() {
        if (this._holder === null || this._holder === undefined) {
            throw new Error(`Text holder is not inited`);
            return null;
        }
        return this._convertor.js2xml(this._holder);
    }

    static defineElement(obj, name, isCDATA = false) {
        Object.defineProperty(obj, name, {
            get: function() {
                if (isCDATA) {
                    if (obj._holder.xml[name] === null || obj._holder.xml[name] === undefined) {
                        return obj._holder.xml[name];
                    } else {
                        return obj._holder.xml[name].toString();
                    }
                } else {
                    return obj._holder.xml[name];
                }
            },
            set: function(newValue) {
                if (isCDATA) {
                    if (obj._holder.xml[name] === null || obj._holder.xml[name] === undefined) {
                        obj._holder.xml[name] = {};
                    }
                    obj._holder.xml[name].__cdata = newValue;
                } else {
                    obj._holder.xml[name] = newValue;
                }
            }
        });
    }

    static defineChildElement(obj, father, name) {
        Object.defineProperty(obj, name, {
            get: function() {
                if (this._holder.xml[father] !== null && this._holder.xml[father] !== undefined) {
                    if (this._holder.xml[father][name] !== null && this._holder.xml[father][name] !== undefined) {
                        return this._holder.xml[father][name].__cdata;
                    }
                } else {
                    return null;
                }
            },
            set: function(newValue) {
                if (this._holder.xml[father] === null || this._holder.xml[father] === undefined) {
                    this._holder.xml[father] = {};
                }

                if (this._holder.xml[father][name] === null || this._holder.xml[father][name] === undefined) {
                    this._holder.xml[father][name] = {};
                }

                this._holder.xml[father][name].__cdata = v;
            }
        });
    }
}