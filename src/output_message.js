import * as Schema from './message_schema';

export class Text extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'Content', true);
    }
}

export class Image extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineChildElement(this, 'Image', 'MediaId');
    }
}

export class Voice extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineChildElement(this, 'Voice', 'MediaId');
    }
}


export class Video extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineChildElement(this, 'Video', 'MediaId');
        Schema.Schema.defineChildElement(this, 'Video', 'Title');
        Schema.Schema.defineChildElement(this, 'Video', 'Description');
    }
}

export class Music extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineChildElement(this, 'Music', 'Title');
        Schema.Schema.defineChildElement(this, 'Music', 'Description');
        Schema.Schema.defineChildElement(this, 'Music', 'MusicUrl');
        Schema.Schema.defineChildElement(this, 'Music', 'HQMusicUrl');
        Schema.Schema.defineChildElement(this, 'Music', 'ThumbMediaId');
    }
}

export class News extends Schema.Schema {
    constructor() {
        super();
        this._holder.xml.Articles = {};
        this.clearArticles();
        Schema.Schema.defineElement(this, 'ToUserName', true);
        Schema.Schema.defineElement(this, 'FromUserName', true);
        Schema.Schema.defineElement(this, 'CreateTime', false);
        Schema.Schema.defineElement(this, 'MsgType', true);
        Schema.Schema.defineElement(this, 'ArticleCount', false);

    }

    get Articles() {
        const result = [];
        for (let article of this._holder.xml.Articles.item) {
            result.push({
                Title: article.Title,
                Description: article.Description,
                PicUrl: article.PicUrl,
                Url: article.Url
            });
        }
        return result;
    }

    addArticle(title, description, picUrl, url) {
        const article = {};
        if (title !== null && title !== undefined) {
            article.Title = {
                __cdata: title
            };
        }

        if (description !== null && description !== undefined) {
            article.Description = {
                __cdata: description
            };
        }

        if (picUrl !== null && picUrl !== undefined) {
            article.PicUrl = {
                __cdata: picUrl
            };
        }

        if (url !== null && url !== undefined) {
            article.Url = {
                __cdata: url
            };
        }

        this._holder.xml.Articles.item.push(article);
    }

    clearArticles() {
        this._holder.xml.Articles.item = [];
    }
}
