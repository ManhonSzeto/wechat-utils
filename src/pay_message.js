import * as Schema from './message_schema';

export class UnifiedOrderRequest extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'appid');
        Schema.Schema.defineElement(this, 'mch_id');
        Schema.Schema.defineElement(this, 'device_info');
        Schema.Schema.defineElement(this, 'nonce_str');
        Schema.Schema.defineElement(this, 'sign');
        Schema.Schema.defineElement(this, 'body');
        Schema.Schema.defineElement(this, 'detail', true);
        Schema.Schema.defineElement(this, 'attach');
        Schema.Schema.defineElement(this, 'out_trade_no');
        Schema.Schema.defineElement(this, 'fee_type');
        Schema.Schema.defineElement(this, 'total_fee');
        Schema.Schema.defineElement(this, 'spbill_create_ip');
        Schema.Schema.defineElement(this, 'time_start');
        Schema.Schema.defineElement(this, 'time_expire');
        Schema.Schema.defineElement(this, 'goods_tag');
        Schema.Schema.defineElement(this, 'notify_url');
        Schema.Schema.defineElement(this, 'trade_type');
        Schema.Schema.defineElement(this, 'product_id');
        Schema.Schema.defineElement(this, 'limit_pay');
        Schema.Schema.defineElement(this, 'openid');
    }
}

export class UnifiedOrderResponse extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'return_code', true);
        Schema.Schema.defineElement(this, 'return_msg', true);
        Schema.Schema.defineElement(this, 'appid', true);
        Schema.Schema.defineElement(this, 'mch_id', true);
        Schema.Schema.defineElement(this, 'device_info', true);
        Schema.Schema.defineElement(this, 'nonce_str', true);
        Schema.Schema.defineElement(this, 'sign', true);
        Schema.Schema.defineElement(this, 'err_code', true);
        Schema.Schema.defineElement(this, 'err_code_des', true);
        Schema.Schema.defineElement(this, 'result_code', true);
        Schema.Schema.defineElement(this, 'trade_type', true);
        Schema.Schema.defineElement(this, 'prepay_id', true);
        Schema.Schema.defineElement(this, 'code_url', true);
    }
}

export class UnifiedOrderNotification extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'return_code', true);
        Schema.Schema.defineElement(this, 'return_msg', true);
        Schema.Schema.defineElement(this, 'appid', true);
        Schema.Schema.defineElement(this, 'mch_id', true);
        Schema.Schema.defineElement(this, 'device_info', true);
        Schema.Schema.defineElement(this, 'nonce_str', true);
        Schema.Schema.defineElement(this, 'sign', true);
        Schema.Schema.defineElement(this, 'result_code', true);
        Schema.Schema.defineElement(this, 'err_code', true);
        Schema.Schema.defineElement(this, 'err_code_des', true);
        Schema.Schema.defineElement(this, 'openid', true);
        Schema.Schema.defineElement(this, 'is_subscribe', true);
        Schema.Schema.defineElement(this, 'trade_type', true);
        Schema.Schema.defineElement(this, 'bank_type', true);
        Schema.Schema.defineElement(this, 'total_fee');
        Schema.Schema.defineElement(this, 'settlement_total_fee');
        Schema.Schema.defineElement(this, 'fee_type', true);
        Schema.Schema.defineElement(this, 'cash_fee');
        Schema.Schema.defineElement(this, 'cash_fee_type', true);
        Schema.Schema.defineElement(this, 'coupon_fee');
        Schema.Schema.defineElement(this, 'coupon_count');
        Schema.Schema.defineElement(this, 'transaction_id', true);
        Schema.Schema.defineElement(this, 'out_trade_no', true);
        Schema.Schema.defineElement(this, 'attach', true);
        Schema.Schema.defineElement(this, 'time_end', true);
    }

    coupon_type_n(n) {
        return obj._holder.xml[`coupon_type_${n}`];
    }

    coupon_id_n(n) {
        if (obj._holder.xml[`coupon_id_${n}`] === null || obj._holder.xml[`coupon_id_${n}`] === undefined) {
            return obj._holder.xml[`coupon_id_${n}`];
        } else {
            return obj._holder.xml[`coupon_id_${n}`].toString();
        }
    }

    coupon_fee_n(n) {
        return obj._holder.xml[`coupon_fee_${n}`];
    }
}

export class UnifiedOrderNotificationResponse extends Schema.Schema {
    constructor() {
        super();
        Schema.Schema.defineElement(this, 'return_code', true);
        Schema.Schema.defineElement(this, 'return_msg', true);
    }
}
